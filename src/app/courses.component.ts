import {Component} from '@angular/core';

@Component({
  selector: 'courses',
  template: `
    {{ text | summary: 10 }} <br/>
    `
})
export class CoursesComponent {
  text = "Football is changing with the arrival of a new generation of athletes and consumers. The world, which will find a solution to manage the pandemic, has already changed and is evolving. Juventus will be able to keep up."
}
